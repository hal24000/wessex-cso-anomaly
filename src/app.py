import streamlit as st

from pages.page_main import run_page_main
from setup.layout import setup_page

setup_page("Dimension StUI Template")

run_page_main()

with st.sidebar:
    st.header("")
    st.info(
        """
    ### :information_source: Info
    - Info.
    - Info.
    """
    )
