import glob
import re
import sys

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
from tqdm import tqdm

from setup.database import db_con


db = db_con()


def ingest_e_numbers(csv_folder_path, output_name):
    """
    Takes CSVs of CSO EDM E-number data and writes into Mongo DB all the values.
    Raw XLS files were initially converted to individual CSVs.

    Args:
        csv_folder (str): Folder containing all CSVs to ingest. E.g. "../data/kk_ww_published/apr_2017_csv/"
        output_name (str): Name of collection. E.g. 'WESSEX_E_Numbers_Apr_2017_Raw'

    Returns:
        Mongo collection

    """
    # clear collection
    collection = db[output_name]
    print("\n Initial document count:", collection.estimated_document_count())
    collection.drop()
    print("\n Cleared document count:", collection.estimated_document_count())
    all_files = glob.glob(csv_folder_path + "/*.csv")
    file_count = 1
    print(f"\n Total files: {len(all_files)} \n")
    for file in all_files:
        sys.stdout.write(f"\r Processing file {file_count}: {file}")
        df = pd.read_csv(file, header=None)
        df.columns = ["EVALUE_ID", "Datetime", "Level"]
        df["Datetime"] = pd.to_datetime(df["Datetime"], format="%d/%m/%Y %H:%M")
        df = df.sort_values("Datetime").reset_index(drop=True)
        df["Datetime"] = df["Datetime"].dt.strftime("%Y-%m-%d %H:%M:%S")
        collection.insert_many(df.to_dict(orient="records"))
        file_count += 1
    print("\n End document count:", collection.estimated_document_count())


def resample_e_numbers(
    input_name, resample_bool, resample_time, resample_method, output_name
):
    """
    Takes level data for all CSOs (i.e.E-numbers). Resamples to specified time period. Imputes missing values using the median.
    Creates and saves pivoted table for further modelling.
    Will not resample or clean if resample_bool is set to False
    E.g. resample_e_numbers('WESSEX_E_Numbers_Apr_2019_Raw', True, "60Min", "mean", 'WESSEX_E_Numbers_Apr_2019_60Min_Mean')
         resample_e_numbers('WESSEX_E_Numbers_Apr_2019_Raw', False, "na", "na", 'WESSEX_E_Numbers_Apr_2019_Pivot')

    Args:
        input_name (str): Name of source mongo collection
        resample_bool (bool): Boolean whether to resample or not
        resample_time (str): Time period to resample data by
        resample_method (str): Method of resampling
        output_name (str): Name of output mongo collection

    Returns:
        Mongo DB collection

    """
    print("\n Initial document count:", db[output_name].estimated_document_count())
    db[output_name].drop()
    print("\n Cleared document count:", db[output_name].estimated_document_count())
    edm_info = pd.DataFrame(db["WESSEX_CSO_EDM_INFO"].find({}, {"_id": 0}))
    e_numbers = edm_info["SCOPE E -NUMBER"].str.extract("(\d+)").astype(int)[0].tolist()
    mapping_dict = dict(zip(e_numbers, edm_info["SITE_ID"]))

    file_count = 1
    print(f"\n Total entries: {len(mapping_dict)}")
    print(f"\n Writing to: {output_name} \n")
    res_df = pd.DataFrame()
    for e_number in tqdm(mapping_dict):
        sys.stdout.write(f"\r Processing EVALUE_ID {file_count}: {e_number}")
        df = pd.DataFrame(db[input_name].find({"EVALUE_ID": e_number}, {"_id": 0}))
        file_count += 1
        if len(df) > 0:
            df["Datetime"] = pd.to_datetime(df["Datetime"], format="%Y/%m/%d %H:%M:%S")
            df = df.set_index("Datetime")
            if resample_bool == True:
                if resample_method == "mean":
                    resample_df = df.resample(resample_time).mean()
                elif resample_method == "max":
                    resample_df = df.resample(resample_time).max()
                # Impute missing index values
                resample_df.loc[resample_df.isna()["Level"], "EVALUE_ID"] = e_number
                resample_df.loc[resample_df.isna()["Level"], "Level"] = np.nanmedian(
                    resample_df["Level"]
                )
                # Fix instances of repeated values due to ladder bouncing etc
                resample_df["Level_prime"] = np.gradient(resample_df["Level"])
                resample_df["Level_shift+1_prime"] = np.gradient(
                    resample_df["Level"].shift(1)
                )
                resample_df["Level_shift+2_prime"] = np.gradient(
                    resample_df["Level"].shift(2)
                )
                resample_df["Level_shift-1_prime"] = np.gradient(
                    resample_df["Level"].shift(-1)
                )
                resample_df["Level_shift-2_prime"] = np.gradient(
                    resample_df["Level"].shift(-2)
                )
                mask = (
                    (resample_df["Level_prime"] == 0)
                    | (resample_df["Level_shift+1_prime"] == 0)
                    | (resample_df["Level_shift-1_prime"] == 0)
                    | (resample_df["Level_shift+2_prime"] == 0)
                    | (resample_df["Level_shift-2_prime"] == 0)
                )
                resample_df.loc[mask, "Level"] = np.median(df["Level"])
                resample_df["id"] = mapping_dict[e_number]
                df = resample_df[["id", "Level"]]
            else:
                df["id"] = mapping_dict[e_number]
                df = df[["id", "Level"]]
            res_df = pd.concat([res_df, df])
        else:
            pass
    pivot_df = res_df.pivot_table(
        values="Level", columns="id", index="Datetime"
    ).reset_index()
    pivot_df.columns = pivot_df.columns.astype("str")
    pivot_df = pivot_df.fillna(method="ffill").fillna(method="bfill")

    db[output_name].insert_many(pivot_df.to_dict("records"))
    db[output_name].create_index("Datetime")
    print("\n End document count:", db[output_name].estimated_document_count())


def plot_data_availability(mongo_collection):
    """
    Plots data available by sensor, and by date for a collection

    Args:
        mongo_collection (str): Mongo collection to plot

    Return:
        Plots showing data availability by sensor and by time

    """
    df = pd.DataFrame(db[mongo_collection].find({}, {"_id": 0}))
    df["Datetime"] = pd.to_datetime(df["Datetime"])
    df = df.set_index("Datetime")

    na_pct = 100 * df.notna().sum() / len(df)
    fig, ax = plt.subplots(figsize=(25, 8))
    sns.barplot(x=na_pct.index, y=na_pct, ax=ax)
    ax.xaxis.set_tick_params(rotation=90)
    plt.title("% data reported during period, by sensor", fontsize=20)
    fig.show()

    date_na_pct = 100 * df.notna().sum(axis=1) / len(df.columns)
    fig, ax = plt.subplots(figsize=(25, 8))
    sns.lineplot(x=date_na_pct.index, y=date_na_pct, ax=ax)
    ax.xaxis.set_tick_params(rotation=90)
    plt.title("% data reported during period, by date", fontsize=20)
    fig.show()


def ingest_rain(input_csv, output_name, start_date, end_date):
    """
    Generates dataframe of raw rain data for given time period

    Args:
        input_csv (str): Path to input CSV. Data downloaded from Visual Crossing Weather. E.g. "../data/lyneham_apr_2019_rain.csv"
        output_name (str): Name of output collection. E.g. "WESSEX_Rain_Apr_2019_Raw"
        start_date (str): Start date to slice rain. E.g. "2019-04-01 00:00:00"
        end_date (str): End date to slice rain. E.g. "2020-03-18 23:00:00"

    Returns:
        Mongo collection

    """
    df = pd.read_csv("../data/lyneham_apr_2019_rain.csv")
    df = df[["Date time", "Precipitation"]]
    df["Datetime"] = pd.to_datetime(df["Date time"], format="%m/%d/%Y %H:%M:%S")
    df = df.drop("Date time", axis=1)
    df = df.set_index("Datetime").sort_index()
    df = df[start_date:end_date]
    df = df.reset_index()

    collection = db[output_name]
    collection.drop()
    print("\n Initial document count:", collection.estimated_document_count())
    collection.insert_many(df.to_dict("records"))
    collection.create_index("Datetime")
    print("\n End document count:", collection.estimated_document_count())


def process_sites(output_name):
    """
    Generates dataframe that has all useful information for sites. E.g. Site ID, Lat, Lon, node type, node limit
    E.g. process_sites("WESSEX_Site_Info")

    Args:
        output_name (str): Name of output E.g. "WESSEX_Site_Info"

    Returns:
        Mongo collection

    """
    locations = pd.DataFrame(
        db["OLD_WESSEX-WATER_BATH_CSO_SPS_LOCATIONS_WITHLATLONS"].find({}, {"_id": 0})
    )
    edm_info = pd.DataFrame(db["WESSEX_CSO_EDM_INFO"].find({}, {"_id": 0}))
    edm_info["SITE_ID"] = edm_info["SITE_ID"].astype("str")
    edm_info["Spill Level (mm)"] = edm_info["Spill Level (mm)"].apply(
        lambda x: pd.to_numeric(x, errors="coerce")
    )
    spill_level_dict = dict(zip(edm_info["SITE_ID"], edm_info["Spill Level (mm)"]))

    pos_df = (
        locations[["Site_ID", "X_redacted", "Y_redacted", "lat", "lon"]]
        .sort_values("Site_ID")
        .reset_index(drop=True)
    )
    pos_df["Site_ID"] = pos_df["Site_ID"].astype(str)
    pos_df = pos_df.append(
        {
            "Site_ID": "WRC",
            "X_redacted": 369100,
            "Y_redacted": 167500,
            "lat": 51.405042,
            "lon": -2.4408575,
        },
        ignore_index=True,
    )
    # change one node location, otherwise network map breaks
    #     pos_df.loc[pos_df["Site_ID"] == "16089", "X_redacted"] = 375500
    #     pos_df.loc[pos_df["Site_ID"] == "16089", "Y_redacted"] = 164500

    # these nodes have cso data completely missing
    #     no_cso_data_list = []
    #     no_cso_data_list = [
    #         "16052",
    #         "16099",
    #         "16115",
    #         "16755",
    #         "16790",
    #         "17510",
    #         "17531",
    #         "17533",
    #     ]

    df = pd.read_csv(
        "../data/CSO-map.csv",
        usecols=["id", "type", "down_stream", "storm_tank", "inflow"],
    )
    df = df.dropna().sort_values("id").reset_index(drop=True)
    df = df.rename(columns={"id": "Site_ID"})
    # df.loc[df["Site_ID"].isin(no_cso_data_list), "type"] = "NoDataCSO"
    df["Site_ID"] = df["Site_ID"].astype(str)
    df["down_stream"] = df["down_stream"].astype(str)

    df = df.merge(pos_df)
    df["spill_level_mm"] = df["Site_ID"].map(spill_level_dict)
    df["lat"] = df["lat"].values + np.random.normal(0, 0.001, df["lat"].shape)
    df["lon"] = df["lon"].values + np.random.normal(0, 0.001, df["lat"].shape)

    collection = db[output_name]
    collection.drop()
    print("\n Initial document count:", collection.estimated_document_count())
    collection.insert_many(df.to_dict("records"))
    print("\n End document count:", collection.estimated_document_count())


def process_rain(input_rain, output_name):
    """
    Takes resampled flow data and merges with appropriate resampled/modified rain data
    E.g. process_rain('WESSEX_Rain_Apr_2019_Raw', 'WESSEX_Rain_Apr_2019_Processed')

    Args:
        input_rain (str): Name of input rain collection. E.g. 'WESSEX_Rain_Apr_2019_Raw'
        output_name (str): Name of output collection. E.g. 'WESSEX_Rain_Apr_2019_Processed'

    Returns:
        Mongo collection

    """
    rain_df = pd.DataFrame(db[input_rain].find({}, {"_id": 0}))
    rain_df["rain_1h"] = rain_df["Precipitation"].shift(-1).fillna(method="ffill")
    rain_df = rain_df[~rain_df["Datetime"].duplicated(keep="first")].reset_index(
        drop=True
    )
    rain_df["rain_7h_cent_mean"] = (
        rain_df["rain_1h"]
        .rolling(7, center=True)
        .mean()
        .fillna(method="ffill")
        .fillna(method="bfill")
    )
    rain_df["rain_7h_cent_max"] = (
        rain_df["rain_1h"]
        .rolling(7, center=True)
        .max()
        .fillna(method="ffill")
        .fillna(method="bfill")
    )
    rain_df["rain_5h_cent_mean"] = (
        rain_df["rain_1h"]
        .rolling(5, center=True)
        .mean()
        .fillna(method="ffill")
        .fillna(method="bfill")
    )
    rain_df = round(rain_df, 2)
    rain_df = rain_df.drop("Precipitation", axis=1)

    collection = db[output_name]
    print("\n Initial document count:", collection.estimated_document_count())
    collection.drop()
    print("\n Cleared document count:", collection.estimated_document_count())
    collection.insert_many(rain_df.to_dict(orient="records"))
    collection.create_index("Datetime")
    print("\n End document count:", collection.estimated_document_count())


def ingest_b_numbers(csv_folder_path, output_name):
    """
    Takes CSVs of CSO B-number data and writes into Mongo DB all the values.
    Raw XLS files were initially converted to individual CSVs.

    Args:
        csv_folder (str): Folder containing all CSVs to ingest. E.g. "../data/b_apr_2017/"
        output_name (str): Name of collection. E.g. 'WESSEX_B_Numbers_Apr_2017_Raw'

    Returns:
        Mongo collection

    """
    # clear collection
    collection = db[output_name]
    print("\n Initial document count:", collection.estimated_document_count())
    collection.drop()
    print("\n Cleared document count:", collection.estimated_document_count())
    all_files = glob.glob(csv_folder_path + "/*.csv")
    file_count = 1
    print(f"\n Total files: {len(all_files)} \n")

    sps_info = pd.DataFrame(db["WESSEX-WATER_BATH_CSO_SPS_INFO"].find({}, {"_id": 0}))
    sps_info["BVALUE_ID"] = sps_info["Run/Stop B Value"].apply(
        lambda x: re.findall("\d+", x)[0]
    )
    sps_info["Site ID"] = sps_info["Site ID"].astype(str)
    site_info = pd.DataFrame(db["WESSEX_Site_Info"].find({}, {"_id": 0}))

    df_all = pd.DataFrame()
    for file in all_files:
        sys.stdout.write(f"\r Processing file {file_count}: {file}")
        df = pd.read_csv(file, header=None)
        df.columns = ["BVALUE_ID", "Datetime", "Bool_status", "String_status"]
        df["Datetime"] = pd.to_datetime(df["Datetime"], format="%d/%m/%Y %H:%M")
        df["BVALUE_ID"] = df["BVALUE_ID"].astype("str")
        df["Site_ID"] = df["BVALUE_ID"].map(
            dict(zip(sps_info["BVALUE_ID"], sps_info["Site ID"]))
        )
        df["Type"] = df["Site_ID"].map(
            dict(zip(site_info["Site_ID"], site_info["type"]))
        )
        df = (
            df.sort_values("Datetime")
            .reset_index(drop=True)
            .reindex(
                columns=[
                    "Datetime",
                    "BVALUE_ID",
                    "Site_ID",
                    "Type",
                    "Bool_status",
                    "String_status",
                ]
            )
        )

        df_all = pd.concat([df_all, df])
        file_count += 1

    df_all = df_all.sort_values("Datetime")
    collection.insert_many(df_all.to_dict(orient="records"))
    print("\n End document count:", collection.estimated_document_count())


def ingest_sps_info():
    sps_info = pd.DataFrame(
        db["OLD_WESSEX-WATER_BATH_CSO_SPS_INFO"].find({}, {"_id": 0})
    )
    sps_info["Bvalue_ID"] = sps_info["Run/Stop B Value"].apply(
        lambda x: re.findall("\d+", x)[0]
    )
    sps_info["Site_ID"] = sps_info["Site ID"].astype(str)
    sps_info = sps_info.drop("Site ID", axis=1)
    db["WESSEX_SPS_Info"].insert_many(sps_info.to_dict("records"))
