def find_all_incidents():
    """
    Iterates through all CSOs for one year from Apr 2019 - Mar 2020, finding all incidents

    Args:
        None

    Returns:
        all_summary.csv (CSV): CSV of all incidents

    """
    start_date = datetime.strptime("2019/04/01", "%Y/%m/%d")
    node_list = (
        pd.DataFrame(db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find({}, {"_id": 0}))
        .set_index("Datetime")
        .columns
    )

    all_summary_df = pd.DataFrame()
    fortnight_list = []
    for i in range(26):
        fortnight_list.append(start_date + timedelta(days=14 * i))

    for target in node_list:
        upstream, downstream, cso_cluster = find_cso_cluster(target)
        for start_date in fortnight_list:
            try:
                (
                    res_df,
                    alarms_list,
                    two_week_fig,
                    regressor_fig,
                ) = new_plot_two_week_forecast(
                    target, upstream, downstream, cso_cluster, start_date
                )
                incidents_df, summary_df = make_incidents_df(target, alarms_list)
                all_summary_df = pd.concat([all_summary_df, summary_df])
            except:
                pass
    all_summary_df.to_csv("all_summary.csv")
