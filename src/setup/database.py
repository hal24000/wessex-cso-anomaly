import pymongo
from pymongo import MongoClient


def db_con() -> pymongo.database.Database:
    """Connection to Mongo database."""
    db_uri = "mongodb+srv://cd_waterdemo:jA0VmwEW86MAVb8MhiHiJokvvJuYUFp9@atlas-prod-pl-0.hldmf.mongodb.net/cd_waterdemo"
    client = MongoClient(db_uri)
    db_con = client["cd_waterdemo"]
    return db_con
