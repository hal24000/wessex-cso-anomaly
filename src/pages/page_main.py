from datetime import datetime

import pandas as pd
import streamlit as st

from process.utility import (
    color_risk,
    find_sites,
    find_asset_info,
    find_cso_cluster,
)
from visualization.network import plot_map, plot_network
from visualization.plot import (
    plot_forecast,
    plot_rain,
    plot_year_heatmap,
)
from visualization.table import make_incidents_summary

def run_page_main():
    sites_with_data, sites_with_no_data = find_sites()

    c0_1, c0_2, c0_3, c0_4, c0_5, c0_6, c0_7, c0_8 = st.columns(8)
    with c0_5:
        model_option = st.selectbox("Model", ["GAM", "Neural Network"])
        selected_model = (
            "prophet"
            if model_option == "GAM"
            else "neural_prophet"
            if model_option == "Neural Network"
            else None
        )
    with c0_6:
        map_option = st.selectbox("Map", ["Default", "Connected"])
    with c0_7:
        start_time = st.date_input(
            label="Date",
            value=datetime.strptime("2019/12/19", "%Y/%m/%d"),
            min_value=datetime.strptime("2019/04/08", "%Y/%m/%d"),
            max_value=datetime.strptime("2020/03/15", "%Y/%m/%d"),
        )
    with c0_8:
        target = st.selectbox("CSO", sites_with_data, index=28)  # i=79, 72

    ################### Functions ###################
    upstream, downstream, cso_cluster = find_cso_cluster(target)
    pred_fig, regressor_fig = plot_forecast(
        target=target,
        start_time=start_time,
        historic_hours=24 * 7,
        forecast_hours=23,
        model=selected_model,
    )
    rain_fig = plot_rain(start_time)
    incidents_summary = make_incidents_summary(start_time)
    cso_type, cso_lat, cso_lon = find_asset_info(target)
    asset_data = {
        "Asset": [target],
        "Catchment": "Bath",
        "Asset type": [cso_type],
        "Latitude": [cso_lat],
        "Longitude": [cso_lon],
    }
    asset_df = pd.DataFrame(asset_data).set_index("Asset")
    incident_fig = plot_year_heatmap()
    real_map = plot_map(target, incidents_summary)
    network_map = plot_network(target)
    ###################################################

    c1_1, c1_2 = st.columns(2)
    with c1_1:
        with st.expander("Incident Detection", expanded=True):
            st.dataframe(incidents_summary.style.applymap(color_risk, subset=["Risk"]))
        with st.expander("Actual + Predicted Levels", expanded=True):
            st.plotly_chart(pred_fig, use_container_width=True)
    with c1_2:
        with st.expander("Asset Locations", expanded=True):
            if map_option == "Default":
                st.plotly_chart(real_map, use_container_width=True)
            if map_option == "Network":
                st.plotly_chart(network_map, use_container_width=True)
            st.dataframe(asset_df)
    c2_1, c2_2 = st.columns(2)
    with c2_1:
        with st.expander("Upstream + Downstream + Rain", expanded=True):
            st.plotly_chart(regressor_fig, use_container_width=True)
            st.plotly_chart(rain_fig, use_container_width=True)
    with c2_2:
        with st.expander("Year-Long Analysis", expanded=False):
            st.plotly_chart(incident_fig, use_container_width=True)
