# with c0_1:
#     select_section = st.selectbox(
#         "View", ["Dashboard", "Incident Analysis", "Alarm Filtering"]
#     )

# if select_section == "Dashboard":

# if select_section == "Incident Analysis":
#     with c0_7:
#         map_option = st.radio("Map", ["Regional", "Connected"],index=1)
#     with c0_8:
#         start_time = st.date_input(
#             label="Date",
#             value=datetime.strptime("2019/12/19", "%Y/%m/%d"),
#             min_value=datetime.strptime("2019/04/01", "%Y/%m/%d"),
#             max_value=datetime.strptime("2020/03/31", "%Y/%m/%d"),
#         )
#     c1_1, c1_2 = st.beta_columns(2)
#     with c1_1:
#         with st.beta_expander("Heatmap of incidents", expanded=True):
#             incident_fig = plot_weekly_incidents()
#             st.plotly_chart(incident_fig, use_container_width=True)
#     with c1_2:
#         with st.beta_expander("Asset Locations", expanded=True):
#             target = st.selectbox("", sites_with_data, index=28)
#             if map_option == "Regional":
#                 st.plotly_chart(plot_real_map(), use_container_width=True)
#             if map_option == "Connected":
#                 st.plotly_chart(plot_sub_network(target), use_container_width=True)
#             cso_type, cso_lat, cso_lon = find_asset_info(target)
#             d = {
#                 "Asset": [target],
#                 "Catchment": "Bath",
#                 "Asset type": [cso_type],
#                 "Latitude": [cso_lat],
#                 "Longitude": [cso_lon],
#             }
#             df = pd.DataFrame(d).set_index("Asset")
#             st.dataframe(df)
#             #### Functions ####
#             upstream, downstream, cso_cluster = find_cso_cluster(target)
#             use_custom_band = True
#             custom_band_pc = 1
#             fig, regressor_fig = plot_past_week(
#                 target,
#                 start_time,
#                 use_custom_band,
#                 custom_band_pc,
#             )
#             ####################
#         with st.beta_expander("Predicted + Actual", expanded=True):
#             st.plotly_chart(fig, use_container_width=True)
#         with st.beta_expander("Upstream + Downstream + Rain", expanded=True):
#             st.plotly_chart(regressor_fig, use_container_width=True)


# if select_section == "Alarm Filtering":
#     month_list = (
#         pd.DataFrame(
#             db["WESSEX_E_Numbers_Apr_2019_60Min_Max"].find(
#                 {}, {"_id": 0, "Datetime": 1}
#             )
#         )
#         .set_index("Datetime")
#         .index.strftime("%Y-%m")
#         .unique()
#     )
#     with c0_6:
#          select_month = st.selectbox("Heatmap Month", month_list)
#     with c0_7:
#         target = st.selectbox("Sensor", sites_with_data, index=28)
#     upstream, downstream, cso_cluster = find_cso_cluster(target)
#     min_start_date, min_end_date = find_min_date(cso_cluster)
#     days_to_show = 3
#     with c0_8:
#         start_date = st.date_input(
#             label="Flow Date",
#             value=datetime.strptime("2019/04/03", "%Y/%m/%d"),
#             min_value=min_start_date,
#             max_value=min_end_date - timedelta(days=days_to_show),
#         )
#     daily_breaches_max = plot_daily_breaches(
#         "WESSEX_E_Numbers_Apr_2019_60Min_Max", select_month
#     )
#     daily_breaches_mean = plot_daily_breaches(
#         "WESSEX_E_Numbers_Apr_2019_60Min_Mean", select_month
#     )

#     fig_max, reg_fig_max = plot_real(
#         "WESSEX_E_Numbers_Apr_2019_60Min_Max",
#         target,
#         upstream,
#         downstream,
#         cso_cluster,
#         start_date,
#         days_to_show,
#     )
#     fig_mean, reg_fig_mean = plot_real(
#         "WESSEX_E_Numbers_Apr_2019_60Min_Mean",
#         target,
#         upstream,
#         downstream,
#         cso_cluster,
#         start_date,
#         days_to_show,
#     )
#     fig_rain = plot_rain(start_date, days_to_show)

#     with st.beta_expander("Limit excessions over one year", expanded=True):
#         c1_1, c1_2 = st.beta_columns((1, 1))
#         with c1_1:
#             st.markdown("**Max resampling**")
#             st.plotly_chart(daily_breaches_max, use_container_width=True)
#             st.plotly_chart(fig_max, use_container_width=True)
#         with c1_2:
#             st.markdown("**Mean resampling**")
#             st.plotly_chart(daily_breaches_mean, use_container_width=True)
#             st.plotly_chart(fig_mean, use_container_width=True)
#     with st.beta_expander("Upstream/downstream", expanded=True):
#         c2_1, c2_2 = st.beta_columns((1, 1))
#         with c2_1:
#             st.plotly_chart(reg_fig_max, use_container_width=True)
#         with c2_2:
#             st.plotly_chart(reg_fig_mean, use_container_width=True)
#     with st.beta_expander("Rain", expanded=True):
#         c3_1, c3_2 = st.beta_columns((1, 1))
#         with c3_1:
#             st.plotly_chart(fig_rain, use_container_width=True)
#         with c3_2:
#             st.plotly_chart(fig_rain, use_container_width=True)
