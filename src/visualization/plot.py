from datetime import timedelta

import numpy as np
import pandas as pd
import plotly.graph_objects as go
from plotly.subplots import make_subplots

from process.utility import find_cso_cluster
from models.model_run import make_forecast
from setup.database import db_con


db = db_con()

def plot_forecast(
    target,
    start_time,
    historic_hours,
    forecast_hours,
    model,
):
    upstream, downstream, cso_cluster = find_cso_cluster(target)
    start_time = pd.to_datetime(start_time)
    historic_start = start_time - timedelta(hours=historic_hours)
    historic_end = start_time - timedelta(hours=1)
    forecast_end = start_time + timedelta(hours=forecast_hours)

    res_df = make_forecast(
        target, start_time, no_of_hours=24, model=model, up_down=False
    )
    df_sites = pd.DataFrame(
        db["WESSEX_Site_Info"].find({}, {"_id": 0, "Site_ID": 1, "spill_level_mm": 1})
    )
    df_sites = df_sites.fillna(0)
    spill_level_dict = dict(zip(df_sites["Site_ID"], df_sites["spill_level_mm"]))
    upstreams = []
    for i in range(len(upstream)):
        try:
            upstreams.append(f"Upstream: {upstream[i]}")
        except:
            pass
    downstreams = []
    for i in range(len(downstream)):
        try:
            downstreams.append(f"Downstream: {downstream[i]}")
        except:
            pass
    subplot_titles = (*upstreams, *downstreams, "Hourly Rainfall (mm)")
    query = {
        "Datetime": {
            "$gte": historic_start,
            "$lte": forecast_end,
        }
    }
    project = {"_id": 0, "Datetime": 1}
    project.update({cso: 1 for cso in cso_cluster})
    real_df = (
        pd.DataFrame(db["WESSEX_E_Numbers_Apr_2019_Pivot"].find(query, project))
        .set_index("Datetime")
        .sort_index()
    )

    if model == "prophet":
        query = {
            "Datetime": {
                "$gte": historic_start,
                "$lte": historic_end,
            }
        }
        project = {
            "_id": 0,
            "Datetime": 1,
            f"{target}_yhat": 1,
            f"{target}_yhat_upper": 1,
            f"{target}_yhat_lower": 1,
        }
        pred_df = (
            pd.DataFrame(
                db["WESSEX_E_Numbers_Apr_2019_60Min_Mean_Prophet_Predictions"].find(
                    query, project
                )
            )
            .set_index("Datetime")
            .sort_index()
        )

        band_scaler = 2
        pred_df[f"{target}_yhat_upper_raw"] = pred_df[f"{target}_yhat_upper"]
        pred_df[f"{target}_yhat_lower_raw"] = pred_df[f"{target}_yhat_lower"]

        pred_df[f"{target}_yhat_upper"] = (
            pred_df[f"{target}_yhat"]
            + (pred_df[f"{target}_yhat_upper_raw"] - pred_df[f"{target}_yhat"])
            * band_scaler
        )
        pred_df[f"{target}_yhat_lower"] = (
            pred_df[f"{target}_yhat"]
            - (pred_df[f"{target}_yhat"] - pred_df[f"{target}_yhat_lower_raw"])
            * band_scaler
        )

    elif model == "neural_prophet":
        pred_df = make_forecast(
            target=target,
            start_time=historic_start,
            no_of_hours=7 * 24,
            model=model,
            up_down=True,
        )
        pred_df = pred_df.rename(columns={"yhat": f"{target}_yhat"})

    res_df = res_df.rename(columns={"yhat": f"{target}_yhat"})
    pred_df = pd.concat([pred_df, res_df])

    fig = make_subplots(
        rows=1,
        cols=1,
        vertical_spacing=0.05,
        subplot_titles=[f"CSO: {target}"],
    )
    fig["layout"].update(
        margin=dict(l=20, r=20, b=0, t=20),
        legend=dict(orientation="h", yanchor="bottom", y=-0.4, xanchor="center", x=0.5),
        height=250,
    )
    fig.add_trace(
        go.Scatter(
            x=real_df[historic_start:forecast_end].index,
            y=round(real_df[historic_start:historic_end][target]),
            name="Actual",
            line_color="orange",
        ),
        row=1,
        col=1,
    )
    fig.add_hline(
        y=spill_level_dict[target],
        line_dash="dash",
        annotation_text=f"Spill level: {int(spill_level_dict[target])}",
        annotation_position="bottom right",
    )

    fig.add_trace(
        go.Scatter(
            x=pred_df[historic_start:forecast_end].index,
            y=round(pred_df[historic_start:forecast_end][f"{target}_yhat"]),
            name=f"Forecast",
            line_color="lightgrey",
        ),
        row=1,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=pred_df.index,
            y=pred_df[f"{target}_yhat_upper"],
            mode="none",
            fill="tonexty",
            fillcolor="rgba(255, 0, 0, 0.3)",
            showlegend=True,
            name="Upper band",
        )
    )
    fig.add_trace(
        go.Scatter(
            x=pred_df[historic_start:forecast_end].index,
            y=pred_df[historic_start:historic_end][f"{target}_yhat_lower"],
            mode="none",
            fill="tonexty",
            fillcolor="rgba(255, 0, 0, 0.3)",
            showlegend=True,
            name="Lower band",
        )
    )
    fig.for_each_trace(
        lambda trace: trace.update(visible="legendonly")
        if trace.name in ["Upper band", "Lower band"]
        else ()
    )
    fig.update_layout(
        height=300,
        width=600,
        legend_traceorder="normal",
    )

    regressor_fig = make_subplots(
        rows=len(upstream + downstream),
        cols=1,
        shared_xaxes=True,
        subplot_titles=subplot_titles,
        vertical_spacing=0.1,
    )
    regressor_fig["layout"].update(
        margin=dict(l=20, r=20, b=0, t=20),
        height=(len(upstream + downstream) * 200),
    )
    if len(upstream + downstream) > 0:
        for i in range(len(upstream)):
            regressor_fig.add_trace(
                go.Scatter(
                    x=real_df[historic_start:forecast_end].index,
                    y=round(real_df[historic_start:historic_end][upstream[i]]),
                    showlegend=False,
                    line_color="slategrey",
                    name="Upstream",
                ),
                row=(1 + i),
                col=1,
            )
            regressor_fig.add_hline(
                y=spill_level_dict[upstream[i]],
                line_dash="dash",
                annotation_text=f"Spill level: {int(spill_level_dict[upstream[i]])}",
                annotation_position="bottom right",
                row=(1 + i),
                col=1,
            )
        for i in range(len(downstream)):
            regressor_fig.add_trace(
                go.Scatter(
                    x=real_df[historic_start:forecast_end].index,
                    y=round(real_df[historic_start:historic_end][downstream[i]]),
                    showlegend=False,
                    line_color="slategrey",
                    name="Downstream",
                ),
                row=(1 + i + len(upstream)),
                col=1,
            )
            regressor_fig.add_hline(
                y=spill_level_dict[downstream[i]],
                line_dash="dash",
                annotation_text=f"Spill level: {int(spill_level_dict[downstream[i]])}",
                annotation_position="bottom right",
                row=(1 + i + len(upstream)),
                col=1,
            )
    regressor_fig.update_xaxes(range=[historic_start, forecast_end])
    return fig, regressor_fig


def plot_rain(start_time):
    start_time = pd.to_datetime(start_time)

    query = {
        "Datetime": {
            "$gte": start_time - timedelta(hours=24 * 7),
            "$lte": start_time + timedelta(hours=23),
        }
    }
    project = {"_id": 0, "Datetime": 1, "rain_1h": 1, "rain_7h_cent_mean": 1}
    rain_df = (
        pd.DataFrame(db["WESSEX_Rain_Apr_2019_Processed"].find(query, project))
        .set_index("Datetime")
        .sort_index()
    )

    historic_start = start_time - timedelta(hours=24 * 7)
    historic_end = start_time - timedelta(hours=1)
    forecast_end = start_time + timedelta(hours=23)

    avg_rain = round(np.mean(rain_df[historic_start:historic_end]["rain_1h"]), 2)

    rain_fig = make_subplots(
        rows=1,
        cols=1,
        vertical_spacing=0.1,
        subplot_titles=["Historic + Forecast Rainfall (mm)"],
    )
    rain_fig.add_trace(
        go.Bar(
            x=rain_df[historic_start:historic_end].index,
            y=rain_df[historic_start:historic_end]["rain_1h"],
            name="1h Rain",
            marker_color="cornflowerblue",
        ),
        row=1,
        col=1,
    )
    rain_fig.add_trace(
        go.Scatter(
            x=rain_df[historic_start:historic_end].index,
            y=[avg_rain] * len(rain_df[historic_start:historic_end].index),
            name="7 Day Mean 1h Rain",
            fillcolor="rgba(0,255,255,0.3)",
            fill="tozeroy",
            mode="none",
        ),
        row=1,
        col=1,
    )
    rain_fig.add_trace(
        go.Bar(
            x=rain_df[start_time:forecast_end].index,
            y=rain_df[start_time:forecast_end]["rain_1h"],
            name="1h Forecast Rain",
            marker_color="darkgrey",
        ),
        row=1,
        col=1,
    )
    rain_fig.add_trace(
        go.Bar(
            x=rain_df[historic_start:forecast_end].index,
            y=rain_df[historic_start:forecast_end]["rain_7h_cent_mean"],
            name="7h Mean Rain",
            marker_color="orange",
        ),
        row=1,
        col=1,
    )
    rain_fig["layout"].update(
        margin=dict(l=20, r=20, b=0, t=20),
        height=200,
        legend=dict(orientation="h", yanchor="bottom", y=-0.4, xanchor="right", x=1),
    )
    rain_fig.for_each_trace(
        lambda trace: trace.update(visible="legendonly")
        if trace.name in ["7h Mean Rain"]
        else ()
    )
    return rain_fig


def plot_year_heatmap():
    """
    Takes Mongo collection of all incidents/exceptions that were predicted to occur for all CSOs during one year. Plots heatmap

    Args:
        None

    Returns:
        fig (plotly.graph_objs._figure.Figure): Heatmap of all incidents

    """
    df = pd.DataFrame(db["WESSEX_Incidents"].find({}, {"_id": 0}))
    df["Date"] = df["Datetime"].dt.date
    df = df.sort_values("Date").reset_index(drop=True)
    df["CSO ID"] = df["CSO ID"].astype("str")
    df["Week commencing"] = df["Date"].apply(
        lambda x: (x - timedelta(days=x.weekday())).strftime("%Y/%m/%d")
    )
    df["Week ending"] = df["Date"].apply(
        lambda x: (x - timedelta(days=x.weekday()) + timedelta(7)).strftime("%Y/%m/%d")
    )
    df_pivot = df.pivot_table(
        values="Duration", index="CSO ID", columns="Week ending", aggfunc=np.sum
    )
    df_pivot = df_pivot.reindex(df["Week ending"].unique().tolist(), axis=1)
    df_pivot = df_pivot.fillna(0)

    fig = go.Figure(
        data=go.Heatmap(
            z=df_pivot,
            x=df_pivot.columns.tolist(),
            y=df_pivot.index,
            colorscale="bupu",
            hovertemplate="Week ending: %{x}<br>CSO: %{y}<br>Exceptions: %{z}<extra></extra>",
        )
    )
    fig.update_layout(
        height=1200,
        width=1000,
        xaxis_title="Week ending",
        yaxis_title="CSO",
        margin=dict(l=100, r=100, b=0, t=20),
        xaxis=dict(tickfont=dict(size=12)),
        yaxis=dict(tickfont=dict(size=10)),
    )
    fig["layout"]["yaxis"]["autorange"] = "reversed"
    return fig
