import plotly.graph_objects as go
from plotly.subplots import make_subplots

from process.utility import find_sites


def plot_errors(mae_list, rmse_list, mape_list):
    target_cols, _ = find_sites()
    fig = make_subplots(
        subplot_titles=["MAE", "RMSE", "MAPE"], rows=3, cols=1, shared_xaxes=True
    )
    fig.add_trace(go.Bar(x=target_cols, y=mae_list, name="MAE"), row=1, col=1)
    fig.add_trace(go.Bar(x=target_cols, y=rmse_list, name="RMSE"), row=2, col=1)
    fig.add_trace(go.Bar(x=target_cols, y=mape_list, name="MAPE"), row=3, col=1)
    return fig


def plot_forecast(target, upstream, downstream, start_date, res_df):
    test_days = 21
    real_days = 14
    test_start_date = pd.date_range(start_date, periods=24 * test_days, freq="H")[0]
    real_end_date = pd.date_range(start_date, periods=24 * real_days, freq="H")[-1]
    test_end_date = pd.date_range(start_date, periods=24 * test_days, freq="H")[-1]
    rain_cols = ["rain_shift-1"]

    # spill levels
    df_sites = pd.DataFrame(
        db["WESSEX_Site_Info"].find({}, {"_id": 0, "Site_ID": 1, "spill_level_mm": 1})
    )
    spill_level_dict = dict(zip(df_sites["Site_ID"], df_sites["spill_level_mm"]))

    upstreams = []
    for i in range(len(upstream)):
        try:
            upstreams.append(f"Upstream: {upstream[i]}")
        except:
            pass
    downstreams = []
    for i in range(len(upstream)):
        try:
            downstreams.append(f"Downstream: {downstream[i]}")
        except:
            pass
    subplot_titles = (
        *upstreams,
        *downstreams,
        *rain_cols,
    )

    # target fig
    fig = make_subplots(
        rows=1,
        cols=1,
        subplot_titles=[f"CSO: {target}"],
        vertical_spacing=0.05,
    )
    fig["layout"].update(
        margin=dict(l=20, r=20, b=0, t=20),
        legend=dict(orientation="h", yanchor="bottom", y=-0.4, xanchor="center", x=0.5),
    )
    fig.add_trace(
        go.Scatter(
            x=res_df[test_start_date:real_end_date].index,
            y=res_df[test_start_date:real_end_date].y,
            name=f"Real",
        ),
        row=1,
        col=1,
    )
    fig.add_trace(
        go.Scatter(
            x=res_df.index,
            y=res_df.yhat,
            name=f"Predicted",
        ),
        row=1,
        col=1,
    )

    fig.add_hline(
        y=spill_level_dict[target],
        line_dash="dash",
        annotation_text=f"WW spill level: {spill_text}",
        annotation_position="bottom right",
    )
    fig.add_trace(
        go.Scatter(
            x=res_df.index,
            y=res_df.yhat_upper,
            mode="none",
            fill="tonexty",
            fillcolor="rgba(255, 0, 0, 0.3)",
            showlegend=True,
            name="Upper band",
        )
    )
    fig.add_trace(
        go.Scatter(
            x=res_df.index,
            y=res_df.yhat_lower,
            mode="none",
            fill="tonexty",
            fillcolor="rgba(255, 0, 0, 0.3)",
            showlegend=True,
            name="Lower band",
        )
    )
    fig.for_each_trace(
        lambda trace: trace.update(visible="legendonly")
        if trace.name in ["Upper band", "Lower band"]
        else ()
    )
    fig.update_layout(
        height=300,
        width=600,
        legend_traceorder="normal",
    )

    # regressors
    regressor_fig = make_subplots(
        rows=len(upstream + downstream + rain_cols),
        cols=1,
        shared_xaxes=True,
        subplot_titles=subplot_titles,
        vertical_spacing=0.05,
    )
    regressor_fig["layout"].update(
        margin=dict(l=20, r=20, b=0, t=20),
    )
    for i in range(len(upstream)):
        regressor_fig.add_trace(
            go.Scatter(
                x=res_df.index,
                y=res_df[upstream[i]],
                line_color="lightslategrey",
                showlegend=False,
            ),
            row=(1 + i),
            col=1,
        )
    for i in range(len(downstream)):
        regressor_fig.add_trace(
            go.Scatter(
                x=res_df.index,
                y=res_df[downstream[i]],
                line_color="lightslategrey",
                showlegend=False,
            ),
            row=(1 + i + len(upstream)),
            col=1,
        )
    regressor_fig.add_trace(
        go.Scatter(
            x=res_df.index,
            y=res_df["rain_shift-1"],
            line_color="brown",
            showlegend=False,
        ),
        row=(1 + len(upstream) + len(downstream)),
        col=1,
    )
    regressor_fig.update_layout(
        width=600,
        legend_traceorder="normal",
    )
    return fig, regressor_fig


def plot_diff(start_date, res_df):
    """
    Using the predicted levels, plots residuals between predicted and actual for two weeks

    Args:
        start_date (pandas._libs.tslibs.timestamps.Timestamp): Date to start prediction
        res_df (pandas.core.frame.DataFrame): Dataframe of predictions

    Returns:
        fig (plotly.graph_objs._figure.Figure): Figure showing two weeks of residuals

    """
    forecast_days = 21
    test_start_date = pd.date_range(start_date, periods=24 * 21, freq="H")[0]
    real_end_date = pd.date_range(start_date, periods=24 * 14, freq="H")[-1]
    test_end_date = pd.date_range(start_date, periods=24 * 21, freq="H")[-1]
    diff_res_df = res_df.copy()
    diff_res_df["y_minus_yhat"] = diff_res_df["y"] - diff_res_df["yhat"]

    fig = make_subplots(
        rows=1, cols=1, vertical_spacing=0.05, subplot_titles=["Residuals"]
    )
    fig.add_trace(
        go.Scatter(
            x=diff_res_df.index,
            y=diff_res_df[test_start_date:real_end_date].y_minus_yhat,
            line_color="green",
            showlegend=False,
        ),
        row=1,
        col=1,
    )
    fig["layout"].update(
        margin=dict(l=20, r=20, b=0, t=20),
        legend=dict(orientation="h", yanchor="bottom", y=-0.4, xanchor="center", x=0.5),
        plot_bgcolor="rgba(173, 216, 230, 0.3)",
        height=300,
        width=600,
        legend_traceorder="normal",
    )
    fig.update_xaxes(range=[test_start_date, test_end_date])
    return fig


def plot_daily_breaches(input_collection, select_month):
    """
    e.g. 'WESSEX_E_Numbers_Apr_2019_60Min_Max'
    """
    query = {}
    project = {"_id": 0, "Site_ID": 1, "spill_level_mm": 1}
    spill_df = (
        pd.DataFrame(db["WESSEX_Site_Info"].find(query, project))
        .dropna()
        .reset_index(drop=True)
    )
    spill_level_dict = dict(zip(spill_df["Site_ID"], spill_df["spill_level_mm"]))
    cso_with_limits = spill_df["Site_ID"].to_list()

    query = {}
    project = {"_id": 0, "Datetime": 1}
    project.update({cso: 1 for cso in cso_with_limits})

    df = pd.DataFrame(db[input_collection].find(query, project))
    df["Datetime"] = pd.to_datetime(df["Datetime"], format="%Y/%m/%d %H:%M:%S")
    df = df.set_index("Datetime")
    df = df.fillna(method="ffill")
    df = df[df.index.year.isin([datetime.strptime(select_month, "%Y-%m").year])]
    df = df[df.index.month.isin([datetime.strptime(select_month, "%Y-%m").month])]

    res_df = df.copy()
    for col in res_df.columns:
        res_df[col] = df[col] > spill_level_dict[col]

    one_day_resample = res_df.resample("1d").sum().T
    one_day_resample = one_day_resample[one_day_resample.sum(axis=1) != 0]

    day_list = [
        datetime.strftime(i, "%Y-%m-%d") for i in one_day_resample.columns.to_list()
    ]
    fig = go.Figure(
        data=go.Heatmap(
            z=one_day_resample,
            x=day_list,
            y=one_day_resample.index,
            colorscale="bupu",
            hovertemplate="Day: %{x}<br>CSO: %{y}<br>Limit breaches: %{z}<extra></extra>",
        )
    )

    fig.update_layout(
        xaxis_title="Day",
        yaxis_title="CSO",
        margin=dict(l=100, r=100, b=0, t=20),
        xaxis=go.layout.XAxis(tickangle=90),
        yaxis=dict(tickfont=dict(size=10)),
    )
    fig["layout"]["yaxis"]["autorange"] = "reversed"
    return fig


def plot_week_max(collection, target, start_date):
    """
    'WESSEX_E_Numbers_Apr_2019_60Min_Max'
    """
    df_sites = pd.DataFrame(
        db["WESSEX_Site_Info"].find({}, {"_id": 0, "Site_ID": 1, "spill_level_mm": 1})
    )
    spill_level_dict = dict(zip(df_sites["Site_ID"], df_sites["spill_level_mm"]))

    query = {"Datetime": {"$gte": start_date, "$lte": start_date + timedelta(days=1)}}
    project = {"_id": 0, "Datetime": 1}
    project.update({target: 1})
    df = (
        pd.DataFrame(db[collection].find(query, project))
        .sort_values("Datetime")
        .set_index("Datetime")
    )

    # target fig
    fig = make_subplots(
        rows=1,
        cols=1,
        subplot_titles=[f"CSO: {target}"],
        vertical_spacing=0.05,
    )
    fig["layout"].update(
        margin=dict(l=20, r=20, b=0, t=20),
        legend=dict(orientation="h", yanchor="bottom", y=-0.4, xanchor="center", x=0.5),
        plot_bgcolor="rgba(173, 216, 230, 0.3)",
    )
    fig.add_trace(
        go.Scatter(
            x=df.index,
            y=df.target,
            name=f"Real",
            line_color="grey",
        ),
        row=1,
        col=1,
    )

    fig.add_hline(
        y=spill_level_dict[target],
        line_dash="dash",
        annotation_text=f"WW spill level: {spill_level_dict[target]}",
        annotation_position="bottom right",
    )
    fig.update_layout(
        height=300,
        width=600,
        legend_traceorder="normal",
    )
    return fig


def plot_real(
    collection, target, upstream, downstream, cso_cluster, start_date, days_to_show
):
    start_date = pd.to_datetime(start_date)
    end_date = pd.date_range(start_date, periods=24 * days_to_show, freq="H")[-1]
    rain_cols = ["rain_shift-1"]

    df_sites = pd.DataFrame(
        db["WESSEX_Site_Info"].find({}, {"_id": 0, "Site_ID": 1, "spill_level_mm": 1})
    )
    spill_level_dict = dict(zip(df_sites["Site_ID"], df_sites["spill_level_mm"]))

    query = {"Datetime": {"$gte": start_date, "$lte": end_date}}
    project = {"_id": 0, "Datetime": 1}
    project.update({cso: 1 for cso in cso_cluster})
    df = (
        pd.DataFrame(db[collection].find(query, project))
        .set_index("Datetime")
        .sort_index()
    )

    upstreams = []
    for i in range(len(upstream)):
        try:
            upstreams.append(f"Upstream: {upstream[i]}")
        except:
            pass
    downstreams = []
    for i in range(len(downstream)):
        try:
            downstreams.append(f"Downstream: {downstream[i]}")
        except:
            pass
    subplot_titles = (*upstreams, *downstreams)

    fig = make_subplots(
        rows=1,
        cols=1,
        subplot_titles=[f"CSO: {target}"],
        vertical_spacing=0.05,
    )
    fig.add_trace(
        go.Scatter(
            x=df.index,
            y=df[target],
            name=f"Real",
        ),
        row=1,
        col=1,
    )
    fig.add_hline(
        y=spill_level_dict[target],
        line_dash="dash",
        annotation_text=f"WW spill level: {spill_level_dict[target]}",
        annotation_position="bottom right",
    )
    fig.update_layout(
        height=300,
        legend=dict(orientation="h", yanchor="bottom", y=-0.4, xanchor="center", x=0.5),
        legend_traceorder="normal",
        margin=dict(l=20, r=20, b=0, t=20),
        plot_bgcolor="rgba(173, 216, 230, 0.3)",
    )

    regressor_fig = make_subplots(
        rows=len(upstream + downstream),
        cols=1,
        shared_xaxes=True,
        subplot_titles=subplot_titles,
        vertical_spacing=0.05,
    )
    regressor_fig["layout"].update(
        margin=dict(l=20, r=20, b=0, t=20),
    )
    for i in range(len(upstream)):
        regressor_fig.add_trace(
            go.Scatter(
                x=df.index,
                y=df[upstream[i]],
                showlegend=False,
            ),
            row=(1 + i),
            col=1,
        )
    for i in range(len(downstream)):
        regressor_fig.add_trace(
            go.Scatter(
                x=df.index,
                y=df[downstream[i]],
                showlegend=False,
            ),
            row=(1 + i + len(upstream)),
            col=1,
        )
    return fig, regressor_fig
