import pandas as pd

from process.utility import process_result
from setup.database import db_con


db = db_con()

def find_incidents(target, alarms_list):
    """
    Given a target CSO and list of alarms triggered, make a dataframe of incidents

    Args:
        target (str): Target CSO
        alarms_list (list[pandas._libs.tslibs.timestamps.Timestamp]): List of timestamps of incidents

    Returns:
        df (pandas.core.frame.DataFrame): Dataframe of all incidents
        summary_df (pandas.core.frame.DataFrame): Condensed dataframe of incidents. Incidents occuring within 3 hours of each other are considered to be the same incident

    """
    df = pd.DataFrame(alarms_list)
    df["CSO ID"] = target
    df["Date"] = df.ds.dt.date
    df["Time"] = df.ds.dt.time
    df["time_shift"] = df["ds"].shift(1)
    df["Incident ID"] = 1

    counter = 1
    for i, row in df[1:].iterrows():
        if (row["ds"].value - row["time_shift"].value) / (360 * 1e10) > 3:
            counter += 1
            df.iloc[i, 5] = counter
        else:
            df.iloc[i, 5] = counter
    df = df.drop(["ds", "time_shift"], axis=1)
    summary_df = df[~df["Incident ID"].duplicated()].set_index("Incident ID")
    summary_df.loc[:, "Duration"] = df.groupby("Incident ID").count()["CSO ID"]
    summary_df["Risk"] = summary_df["Duration"].apply(
        lambda x: "High" if x > 10 else "Med" if 5 < x < 10 else "Low"
    )
    summary_df = summary_df.sort_values("Duration", ascending=False)
    summary_df = summary_df.rename(columns={"Time": "Start time"})
    summary_df.reindex(columns=["CSO ID", "Date", "Start time", "Duration", "Risk"])
    return df, summary_df


def find_yearly_incidents():
    start_date = datetime.strptime("2019/04/08", "%Y/%m/%d")
    node_list = (
        pd.DataFrame(db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find({}, {"_id": 0}))
        .set_index("Datetime")
        .columns
    )

    all_incident_df = pd.DataFrame()
    all_summary_df = pd.DataFrame()
    week_list = []
    for i in range(50):
        week_list.append(start_date + timedelta(days=7 * i))

    for target in tqdm(node_list):
        for start_time in week_list:
            res_df, alarms_list = process_result(
                target=target,
                start_time=start_time,
                use_custom_band=False,
                custom_band_pc=None,
            )
            incident_df, summary_df = find_incidents(target, alarms_list)
            all_incident_df = pd.concat([all_incident_df, incident_df])
            all_summary_df = pd.concat([all_summary_df, summary_df])

    all_summary_df = all_summary_df.reset_index(drop=True)
    # pymongo cannot store just the date, it needs to be a datetime
    all_summary_df["Datetime"] = pd.to_datetime(
        all_summary_df["Date"].astype("str")
        + " "
        + all_summary_df["Start time"].astype("str"),
        format="%Y-%m-%d %H:%M:%S",
    )
    all_summary_df = all_summary_df.drop(["Date", "Start time"], axis=1)
    db["WESSEX_Incidents"].drop()
    db["WESSEX_Incidents"].insert_many(all_summary_df.to_dict("records"))
    return all_incident_df, all_summary_df


def make_incidents_summary(start_time):
    node_list = (
        pd.DataFrame(
            db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find_one({}, {"_id": 0}),
            index=[0],
        )
        .set_index("Datetime")
        .columns
    )
    all_summary_df = pd.DataFrame()
    for target in node_list:
        res_df, alarms_list = process_result(target, start_time)
        incidents_df, summary_df = find_incidents(target, alarms_list)
        all_summary_df = pd.concat([all_summary_df, summary_df.reset_index(drop=True)])

    all_summary_df = all_summary_df.sort_values(
        by=["Duration", "Date", "Start time"], ascending=[False, True, True]
    )
    all_summary_df = all_summary_df[
        all_summary_df["Risk"].isin(["High", "Med"])
    ].reset_index(drop=True)
    return all_summary_df
