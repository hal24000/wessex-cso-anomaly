import networkx as nx
import numpy as np
import pandas as pd
import plotly.express as px
import plotly.graph_objects as go

from process.utility import find_cso_cluster
from setup.database import db_con


db = db_con()

def make_nx_graph():
    """
    Makes networkx graph from CSV with information on all nodes
    Info: Site ID, type of node, downstream nodes, storm tank, inflow, redacted XY coordinates, lat/lon coordinates, spill levels (mm)
    """
    df = pd.DataFrame(db["WESSEX_Site_Info"].find({}, {"_id": 0}))
    DG = nx.DiGraph()
    for i, row in df.iterrows():
        site_id = row["Site_ID"]
        DG.add_node(
            site_id,
            node_type=row["type"],
            storm_tank=row["storm_tank"],
            inflow=row["inflow"],
            x=row["X_redacted"],
            y=row["Y_redacted"],
        )
        if row["down_stream"] != "0":
            DG.add_edge(
                site_id,
                row["down_stream"],
            )
    return DG


def plot_map(target, all_incidents_df):
    site_info = pd.DataFrame(db["WESSEX_Site_Info"].find({}, {"_id": 0, "Site_ID": 1}))
    levels = pd.DataFrame(
        db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find_one({}, {"_id": 0}), index=[0]
    ).set_index("Datetime")
    sites_with_no_data = list(set(site_info["Site_ID"]) - set(levels.columns))
    sites_with_data = [x for x in levels.columns if x not in sites_with_no_data]

    df = pd.DataFrame(db["WESSEX_Site_Info"].find({}, {"_id": 0}))
    med_risk_dict = dict(
        zip(
            all_incidents_df[all_incidents_df["Risk"] == "Med"]["CSO ID"],
            all_incidents_df[all_incidents_df["Risk"] == "Med"]["Risk"],
        )
    )
    high_risk_dict = dict(
        zip(
            all_incidents_df[all_incidents_df["Risk"] == "High"]["CSO ID"],
            all_incidents_df[all_incidents_df["Risk"] == "High"]["Risk"],
        )
    )
    risk_dict = {**med_risk_dict, **high_risk_dict}

    no_data_dict = {x: "SPS/WRC" for x in sites_with_no_data}
    remaining_dict = {
        x: "Low"
        for x in list(
            set(levels.columns)
            - set(list(risk_dict.keys()) + list(no_data_dict.keys()))
        )
    }

    merge_dict = {**risk_dict, **no_data_dict, **remaining_dict}
    risk_number_dict = {"High": 0, "Med": 1, "Low": 2, "SPS/WRC": 3}

    df["Risk"] = df["Site_ID"].map(merge_dict)
    df["Risk_number"] = df["Risk"].map(risk_number_dict)
    df = df.sort_values("Risk_number")
    df = df.reset_index(drop=True)

    fig = px.scatter_mapbox(
        df,
        lat="lat",
        lon="lon",
        hover_name="Site_ID",
        hover_data=["type", "Risk"],
        color="Risk",
        color_discrete_map={
            "High": "red",
            "Med": "orange",
            "Low": "mediumseagreen",
            "SPS/WRC": "grey",
        },
        zoom=11,
        height=588,
        # center=dict(lat = df[df["Site_ID"]==target].iloc[0]['lat'], lon = df[df["Site_ID"]==target].iloc[0]['lon'])
    )
    fig.update_traces(marker_size=8)

    fig.update_layout(
        mapbox_style="carto-positron",
        # mapbox_style="open-street-map",
        margin={"r": 0, "t": 0, "l": 0, "b": 0},
        legend=dict(orientation="h", yanchor="bottom", y=-0.1, xanchor="center", x=0.5),
    )
    return fig


def plot_network(target):
    upstream, downstream, cso_cluster = find_cso_cluster(target)
    DG = make_nx_graph()

    node_x = []
    node_y = []
    node_types = []
    for node in DG.nodes():
        x, y = DG.nodes[node]["x"], DG.nodes[node]["y"]
        node_x.append(x)
        node_y.append(y)
        if node in cso_cluster:
            node_type = 1
            node_types.append(node_type)
        else:
            node_type = 0.5
            node_types.append(node_type)

    node_info = []
    for node in DG.nodes(True):
        node_data = []
        node_data.append(node[0])
        node_data.append(node[1]["node_type"])
        node_info.append(node_data)
    nodes_list = np.array(DG.nodes).tolist()
    text_list = [node if node in cso_cluster else "" for node in nodes_list]
    color = [
        0
        if v == "CSO"
        else 1
        if v == "PumpingStation"
        else 2
        if v == "WaterRecyclingCentre"
        else 3
        for v in list(nx.get_node_attributes(DG, "node_type").values())
    ]

    node_trace = go.Scatter(
        x=node_x,
        y=node_y,
        mode="markers+text",
        hovertext=node_info,
        hoverinfo="text",
        marker=dict(
            color=color,
            colorscale=px.colors.qualitative.D3,
            opacity=node_types,
            size=10,
        ),
        text=text_list,
        textposition="top center",
        textfont_size=20,
    )

    fig = go.Figure(
        data=node_trace,
        layout=go.Layout(
            margin=dict(b=0, l=0, r=0, t=0),
            xaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
            yaxis=dict(showgrid=False, zeroline=False, showticklabels=False),
        ),
    )

    # arrows rather than lines required annotations
    edge_start = []
    edge_end = []
    for edge in DG.edges():
        x0, y0 = DG.nodes[edge[0]]["x"], DG.nodes[edge[0]]["y"]
        x1, y1 = DG.nodes[edge[1]]["x"], DG.nodes[edge[1]]["y"]
        edge_start.append([x0, y0])
        edge_end.append([x1, y1])

    for i in range(len(edge_start)):
        fig.add_annotation(
            ax=edge_start[i][0],  # arrows' tail
            ay=edge_start[i][1],  # arrows' tail
            x=edge_end[i][0],  # arrows' head
            y=edge_end[i][1],  # arrows' head
            xref="x",
            yref="y",
            axref="x",
            ayref="y",
            text="",  # if you want only the arrow
            showarrow=True,
            arrowhead=2,
            arrowsize=2,
            arrowwidth=1,
            arrowcolor="orange",
            opacity=0.7,
        )

    # to set appropriately zoomed in axis on cluster
    cluster_x = []
    cluster_y = []
    for node in cso_cluster:
        cluster_x.append(DG.nodes[node]["x"])
        cluster_y.append(DG.nodes[node]["y"])

    axis_buffer = 250
    fig.update_xaxes(
        range=[min(cluster_x) - axis_buffer, max(cluster_x) + axis_buffer],
        showgrid=False,
    )
    fig.update_yaxes(
        range=[min(cluster_y) - axis_buffer, max(cluster_y) + axis_buffer],
        showgrid=False,
    )

    fig.update_layout(
        height=350,
        width=600,
    )
    return fig
