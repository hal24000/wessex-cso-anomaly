import sys

import numpy as np
import pandas as pd
from prophet import Prophet
from tqdm.notebook import tqdm

from process.utility import find_cso_cluster, find_sites
from setup.database import db_con


db = db_con()

def calculate_prophet_test_errors(use_up_down, train_pc=0.8):
    rain_cols = ["rain_7h_cent_mean"]
    target_cols, _ = find_sites()
    error_dict = {}

    for target in tqdm(target_cols):
        query = {}
        if use_up_down:
            upstream, downstream, cso_cluster = find_cso_cluster(target)
            regressor_list = upstream + downstream + rain_cols
            project = {"_id": 0, "Datetime": 1}
            project.update({cso: 1 for cso in cso_cluster})
        else:
            regressor_list = ["rain_7h_cent_mean"]
            project = {"_id": 0, "Datetime": 1, target: 1}
        df = (
            pd.DataFrame(
                db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find(query, project)
            )
            .set_index("Datetime")
            .sort_index()
        )
        query = {}
        project = {"_id": 0, "Datetime": 1}
        project.update({rain: 1 for rain in rain_cols})
        rain_df = (
            pd.DataFrame(db["WESSEX_Rain_Apr_2019_Processed"].find(query, project))
            .set_index("Datetime")
            .sort_index()
        )
        res_df = pd.concat([df, rain_df], axis=1)
        res_df = res_df.reset_index().rename(columns={"Datetime": "ds", target: "y"})

        split = int(len(res_df) * train_pc)
        train = res_df.iloc[:split]
        test = res_df.iloc[split:]

        # prophet model
        m = Prophet(
            changepoint_prior_scale=0.001,
            seasonality_prior_scale=0.1,
            yearly_seasonality=False,
            interval_width=1,
            changepoint_range=0.1,
        )
        for regressor in regressor_list:
            m.add_regressor(regressor, standardize=True)
        m.fit(train)
        forecast = m.predict(test.drop("y", axis=1))

        error = forecast["yhat"] - test.reset_index(drop=True)["y"]
        ae = np.abs(error)
        mae = np.mean(ae)
        mse = np.mean(np.square(error))
        rmse = np.sqrt(mse)
        mape = np.mean(np.abs(100 * error / test.reset_index(drop=True)["y"]))

        sys.stdout.write(
            f"\r Processed {target}. MAE: {mae}, RMSE: {rmse}, MAPE: {mape}"
        )
        error_dict[target] = [mae, rmse, mape]

    mae_list, rmse_list, mape_list = [], [], []
    for target in target_cols:
        mae_list.append(error_dict[target][0])
        rmse_list.append(error_dict[target][1])
        mape_list.append(error_dict[target][2])
    return mae_list, rmse_list, mape_list
