import json

import pandas as pd
import pickle
from neuralprophet import NeuralProphet
from prophet import Prophet
from prophet.serialize import model_to_json
from tqdm import tqdm

from process.utility import find_cso_cluster
from setup.database import db_con


db = db_con()

def make_prophet(use_up_down, model_name):
    """
    Iterate through all CSOs to generate a prophet model for each.
    make_prophet(False, "prophet_forecast")
    make_prophet(True, "prophet")

    Args:
        use_up_down (bool): Include adjacent network or not.
        model_name (str): Name of model being saved

    Returns:
        Mongo collection

    """
    initial_doc_count = db["WESSEX_Models"].estimated_document_count()
    print(f"Initial doc count: {initial_doc_count}")
    db["WESSEX_Models"].delete_many({"model_name": model_name})
    clean_start_doc_count = db["WESSEX_Models"].estimated_document_count()
    print(f"Clean start doc count: {clean_start_doc_count}")

    target_cols = list(
        db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"]
        .find_one({}, {"_id": 0, "Datetime": 0})
        .keys()
    )
    counter = 0
    for target in target_cols:
        counter += 1
        sys.stdout.write(f"\r Processing {target} ({counter}/{len(target_cols)})")
        train_prophet(target, use_up_down, model_name)

    db["WESSEX_Models"].create_index("sensor_id")
    db["WESSEX_Models"].create_index("model_name")
    db["WESSEX_Models"].create_index("model_start_year")
    end_doc_count = db["WESSEX_Models"].estimated_document_count()
    print(
        f"End doc count: {end_doc_count}. Inserted {end_doc_count - clean_start_doc_count}"
    )


def train_neural_prophet(use_up_down):
    """
    Train neural prophet and produce pickle files.
    E.g. make_prophet(False, "prophet_forecast")

    Args:
        use_up_down (bool): Include adjacent network or not.
        model_name (str): Name of model being saved

    Returns:
        Mongo collection

    """
    target_cols = list(
        db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"]
        .find_one({}, {"_id": 0, "Datetime": 0})
        .keys()
    )
    rain_cols = ["rain_7h_cent_mean"]
    query = {}
    project = {"_id": 0}
    rain_df = pd.DataFrame(
        db["WESSEX_Rain_Apr_2019_Processed"].find(query, project)
    ).set_index("Datetime")

    for target in tqdm(target_cols):
        query = {}
        project = {"_id": 0, "Datetime": 1}

        if use_up_down:
            upstream, downstream, cso_cluster = find_cso_cluster(target)
            project.update({cso: 1 for cso in cso_cluster})
        else:
            project.update({target: 1})
        df = (
            pd.DataFrame(
                db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find(query, project)
            )
            .set_index("Datetime")
            .sort_index()
        )
        train = df.copy()
        train[rain_cols] = rain_df[rain_cols]
        train = train.reset_index().rename(columns={"Datetime": "ds", target: "y"})

        m = NeuralProphet(yearly_seasonality=False)
        if use_up_down:
            regressor_list = upstream + downstream + rain_cols
            for regressor in regressor_list:
                m = m.add_future_regressor(
                    name=regressor,
                    mode="additive",
                )
            pkl_path = f"../src/models/neural_prophet/{target}.pkl"
        else:
            m = m.add_future_regressor(
                name="rain_7h_cent_mean",
                mode="multiplicative",
            )
            pkl_path = f"../src/models/neural_prophet/{target}_forecast.pkl"

        m.fit(train, freq="H")
        with open(pkl_path, "wb") as f:
            pickle.dump(m, f)


def train_prophet(target, use_up_down, model_name):
    """
    Trains a prophet time series model and saves it in serialized format.

    Args:
        target (str): Target CSO

    Returns:
        Prophet model (json): Model serialized in json format

    """
    rain_cols = ["rain_7h_cent_mean"]
    upstream, downstream, cso_cluster = find_cso_cluster(target)

    if use_up_down:
        query = {}
        project = {"_id": 0, "Datetime": 1}
        project.update({cso: 1 for cso in cso_cluster})
        df = (
            pd.DataFrame(
                db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find(query, project)
            )
            .set_index("Datetime")
            .sort_index()
        )
        regressor_list = upstream + downstream + rain_cols
    else:
        query = {}
        project = {"_id": 0, "Datetime": 1, target: 1}
        df = (
            pd.DataFrame(
                db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find(query, project)
            )
            .set_index("Datetime")
            .sort_index()
        )
        regressor_list = rain_cols

    query = {}
    project = {"_id": 0}
    rain_df = pd.DataFrame(
        db["WESSEX_Rain_Apr_2019_Processed"].find(query, project)
    ).set_index("Datetime")

    train = df.copy()
    train[rain_cols] = rain_df[rain_cols]
    train = train.reset_index().rename(columns={"Datetime": "ds", target: "y"})

    m = Prophet(
        changepoint_prior_scale=0.001,
        seasonality_prior_scale=0.1,
        yearly_seasonality=False,
    )
    for regressor in regressor_list:
        m.add_regressor(regressor, standardize=True)
    m.fit(train)

    output = json.loads(model_to_json(m))
    output["sensor_id"] = target
    output["model_name"] = model_name
    output["model_start_year"] = "2019"
    db["WESSEX_Models"].insert_one(output)
