from bson.json_util import dumps
from datetime import datetime, timedelta

import numpy as np
import pandas as pd
import pickle
from prophet.serialize import model_from_json
from tqdm import tqdm

from process.utility import find_cso_cluster
from setup.database import db_con


db = db_con()

def make_forecast(target, start_time, no_of_hours, model, up_down):
    """
    Makes predictions from a start time for set number of hours, using pre-trained model. Can set whether upstream/downstream are included in forecast.
    For historical analysis, upstream/downstream can be included.
    For future forecast, only rain could be used as a regressor.

    Args:
        target (str): Target CSO
        start_date (pandas._libs.tslibs.timestamps.Timestamp): Date to start prediction
        no_of_hours (int): Number of hours to predict
        model (str): Model to be used
        up_down (bool): Use upstream/downstream or not

    Returns:
        res_df (pandas.core.frame.DataFrame): Dataframe of predictions

    """
    query = {
        "Datetime": {
            "$gte": start_time,
            "$lte": start_time + timedelta(hours=no_of_hours),
        }
    }
    project = {"_id": 0}
    rain_df = (
        pd.DataFrame(db["WESSEX_Rain_Apr_2019_Processed"].find(query, project))
        .set_index("Datetime")
        .sort_index()
    )

    if model == "prophet":
        query = {
            "Datetime": {
                "$gte": start_time,
                "$lte": start_time + timedelta(hours=no_of_hours),
            }
        }
        project = {"_id": 0, "Datetime": 1, target: 1}
        df = (
            pd.DataFrame(
                db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find(query, project)
            )
            .set_index("Datetime")
            .sort_index()
        )
        df["rain_7h_cent_mean"] = rain_df["rain_7h_cent_mean"]
        res_df = df.reset_index().rename(columns={"Datetime": "ds", target: "y"})

        cursor = db["WESSEX_Models"].find(
            {
                "sensor_id": target,
                "model_name": "prophet_forecast",
                "model_start_year": "2019",
            },
            {"_id": 0},
        )
        list_cur = list(cursor)[0]
        json_data = dumps(list_cur)
        m = model_from_json(json_data)
        res_df["yhat"] = m.predict(res_df.drop(["y"], axis=1))["yhat"]
        res_df = res_df.set_index("ds")

    if model == "neural_prophet":
        query = {
            "Datetime": {
                "$gte": start_time,
                "$lte": start_time + timedelta(hours=no_of_hours),
            }
        }
        if up_down:
            pkl_path = f"../src/models/neural_prophet/{target}.pkl"
            with open(pkl_path, "rb") as f:
                m = pickle.load(f)
            upstream, downstream, cso_cluster = find_cso_cluster(target)
            project = {"_id": 0, "Datetime": 1}
            project.update({cso: 1 for cso in cso_cluster})

        else:
            pkl_path = f"../src/models/neural_prophet/{target}_forecast.pkl"
            with open(pkl_path, "rb") as f:
                m = pickle.load(f)
            project = {"_id": 0, "Datetime": 1, target: 1}

        df = (
            pd.DataFrame(
                db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find(query, project)
            )
            .set_index("Datetime")
            .sort_index()
        )
        df = df.reset_index().rename(columns={"Datetime": "ds", target: "y"})

        rain_df = rain_df[["rain_7h_cent_mean"]].reset_index(drop=True)
        regressors_df = pd.concat(
            [df.drop(["ds", "y"], axis=1), rain_df["rain_7h_cent_mean"]], axis=1
        )
        temp_df = (df[["ds"]] - timedelta(hours=1)).iloc[[0]]
        temp_df["y"] = 0

        future = m.make_future_dataframe(
            df=temp_df, regressors_df=regressors_df, periods=no_of_hours
        )
        res_df = m.predict(future)
        res_df = res_df.set_index("ds")
        res_df = res_df.rename(columns={"yhat1": "yhat"})
    return res_df


def make_weekly_forecast():
    """
    Using a stored Prophet model, makes a weekly prediction and stores onto Mongo
    Replicates a continuing process, where all data for the year is saved to increase speed of loading
    """
    rain_cols = ["rain_7h_cent_mean"]
    start_time = datetime.strptime("2019/04/08", "%Y/%m/%d")
    no_of_hours = 24 * 7
    node_list = (
        pd.DataFrame(
            db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find_one({}, {"_id": 0}),
            index=[0],
        )
        .set_index("Datetime")
        .columns
    )
    week_list = []
    for i in range(52):
        week_list.append(start_time + timedelta(days=7 * i))

    db["WESSEX_E_Numbers_Apr_2019_60Min_Mean_Prophet_Predictions"].drop()
    for week in tqdm(week_list):
        all_res_df = pd.DataFrame()
        for target in tqdm(node_list):
            upstream, downstream, cso_cluster = find_cso_cluster(target)
            query = {
                "Datetime": {
                    "$gte": week - timedelta(hours=no_of_hours),
                    "$lte": week - timedelta(hours=1),
                }
            }
            project = {"_id": 0, "Datetime": 1}
            project.update({cso: 1 for cso in cso_cluster})
            df = (
                pd.DataFrame(
                    db["WESSEX_E_Numbers_Apr_2019_60Min_Mean"].find(query, project)
                )
                .set_index("Datetime")
                .sort_index()
            )

            query = {
                "Datetime": {
                    "$gte": week - timedelta(hours=no_of_hours),
                    "$lte": week - timedelta(hours=1),
                }
            }
            project = {"_id": 0, "Datetime": 1}
            project.update({rain: 1 for rain in rain_cols})
            rain_df = pd.DataFrame(
                db["WESSEX_Rain_Apr_2019_Processed"].find(query, project)
            ).set_index("Datetime")

            res_df = df.copy()
            res_df[rain_cols] = rain_df[rain_cols]
            res_df = res_df.reset_index().rename(
                columns={"Datetime": "ds", target: "y"}
            )

            cursor = db["WESSEX_Models"].find(
                {
                    "sensor_id": target,
                    "model_name": "prophet",
                    "model_start_year": "2019",
                },
                {"_id": 0},
            )
            list_cur = list(cursor)[0]
            json_data = dumps(list_cur)
            prop = model_from_json(json_data)

            res_df["yhat"] = prop.predict(res_df.drop(["y"], axis=1))["yhat"]
            res_df["yhat_lower"] = prop.predict(res_df.drop(["y"], axis=1))[
                "yhat_lower"
            ]
            res_df["yhat_upper"] = prop.predict(res_df.drop(["y"], axis=1))[
                "yhat_upper"
            ]
            res_df["alarm"] = np.where(
                ((res_df.y < res_df.yhat_lower) | (res_df.y > res_df.yhat_upper)),
                1,
                0,
            )
            res_df = res_df.set_index("ds")
            res_df = res_df.rename_axis("Datetime")
            res_df = res_df[["yhat", "yhat_lower", "yhat_upper", "alarm"]]
            res_df = res_df.rename(
                columns={
                    "yhat": f"{target}_yhat",
                    "yhat_lower": f"{target}_yhat_lower",
                    "yhat_upper": f"{target}_yhat_upper",
                    "alarm": f"{target}_alarm",
                }
            )
            all_res_df = pd.concat([all_res_df, res_df], axis=1)
        db["WESSEX_E_Numbers_Apr_2019_60Min_Mean_Prophet_Predictions"].insert_many(
            all_res_df.reset_index().to_dict(orient="records")
        )
    db["WESSEX_E_Numbers_Apr_2019_60Min_Mean_Prophet_Predictions"].create_index(
        "Datetime"
    )
